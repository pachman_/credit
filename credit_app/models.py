from django.db import models
from django_fsm import FSMField, transition


class Customer(models.Model):
    iin = models.CharField(max_length=12)
    phone_number = models.CharField(max_length=10)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)


class Credit(models.Model):
    MIN_AMOUNT = 5000
    MAX_AMOUNT = 50000
    MIN_DAYS = 5
    MAX_DAYS = 30

    STATUS_CREATED = 'created'
    STATUS_ACTIVE = 'active'
    STATUS_PAID = 'paid'
    STATUS_CHOICES = {
        (STATUS_CREATED, 'Created'),
        (STATUS_ACTIVE, 'Active'),
        (STATUS_PAID, 'Paid')
    }

    date_created = models.DateTimeField(auto_now_add=True, db_index=True)
    amount = models.IntegerField()
    customer = models.ForeignKey(to=Customer, on_delete=models.CASCADE)
    status = FSMField(max_length=15, default=STATUS_CREATED, choices=STATUS_CHOICES, protected=True)


    @transition(field=status, source=STATUS_CREATED, target=STATUS_ACTIVE)
    def to_status_active(self):
        pass

    @transition(field=status, source=STATUS_ACTIVE, target=STATUS_PAID)
    def to_status_paid(self):
        pass