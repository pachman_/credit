# Generated by Django 3.0.4 on 2020-03-13 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('credit_app', '0002_credit'),
    ]

    operations = [
        migrations.AddField(
            model_name='credit',
            name='status',
            field=models.CharField(choices=[('paid', 'Paid'), ('created', 'Created'), ('active', 'Active')], default='created', max_length=15),
        ),
    ]
