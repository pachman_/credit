import pytest
from datetime import datetime
from django.utils import timezone
from credit_app import models



def test_factory_credit(db, credit_factory):
    credit = credit_factory()
    assert isinstance(credit, models.Credit)
    assert credit.status == models.Credit.STATUS_CREATED

def test_credit_change_status_to_active(db, credit_factory):
    credit = credit_factory.active_credit()
    assert credit.status == models.Credit.STATUS_ACTIVE

def test_credit_change_status_to_paid(db, credit_factory):
    credit = credit_factory.paid_credit()
    assert credit.status == models.Credit.STATUS_PAID
