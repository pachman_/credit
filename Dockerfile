FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN mkdir /src
WORKDIR /src
COPY . /src/
RUN find -name "*.pyc" -delete && pip install -U pip && pip install -U -r requirements.txt